package anon.terminalemulator;

import android.os.AsyncTask;
import android.util.Log;

import java9.util.function.Consumer;
import anon.nfccomm.SocketWrapper;
import anon.nfccomm.Transceiver;

import java.io.IOException;
import java.net.ServerSocket;

/**
 * A specific socket wrapper for use by the terminal emulator.
 */
public class TerminalSocketWrapper extends SocketWrapper {

    private final Transceiver trans;
    private AsyncTask<Void, Void, Void> task;

    public TerminalSocketWrapper(ServerSocket serverSocket, Consumer<String> status,
                                 Runnable finishedFunc, Transceiver trans) {
        super(serverSocket, status, finishedFunc);
        this.trans = trans;
    }

    /**
     * The main task to run in the background, sending and receiving commands between the NFC device
     * and the socket.
     */
    public void run() {

        class CardAsyncTask extends AsyncTask<Void, Void, Void> {
            @Override
            public Void doInBackground(Void... params) {
                Log.d(TAG, "Started async task");
                // While not cancelled (outer loop for each connection)...
                while (!isCancelled()) {
                    try {
                        // ...open a new socket connection.
                        open();

                        // While not cancelled (inner loop for each command/response)...
                        while (!isCancelled()) {
                            Log.d(TAG, "entered the receieve message loop.");
                            // ...receive a message, ...
                            byte[] command = receive();
                           // Log.d(TAG, new String (command));
                            if (command == null) {
                                Log.i(TAG, "Terminating connection thread.");
                                break;
                            }
                            Log.d(TAG, "Received command of length "
                                    + command.length);
                            // ...relay it to the device and receive its response...
                            byte[] response = trans.transceive(command);
                            // ...and send the response back through the socket.
                             send(response);
                        }
                    } catch (IOException e) {
                        Log.e(TAG, "There was an error while attempting to "
                                + "communicate with the server:");
                        Log.e(TAG, e.getMessage());
                    }
                    // Close the connection if/when the outer loop has finished sending/receiving
                    // messages.
                    close();
                    Log.d(TAG,"Finished async task");
                }
                return null;
            }
        }
        task = new CardAsyncTask();
        Log.d(TAG, "Starting async task");
        task.execute();
    }

    @Override
    public void close() {
        Log.d(TAG, "Closing TerminalSocketWrapper.");
        if (task != null) {
            task.cancel(true);
        }
        super.close();
        Log.d(TAG, "Closed TerminalSocketWrapper.");
    }

}
