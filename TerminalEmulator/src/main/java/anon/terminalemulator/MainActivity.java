package anon.terminalemulator;

import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NfcAdapter;
import android.nfc.tech.IsoDep;
import android.nfc.tech.NfcA;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;

import anon.nfccomm.Transceiver;
import anon.nfccomm.Utils;

public class MainActivity extends AppCompatActivity {
    // The port on which to open the server socket.
    private static final int PORT = 59557;
    // The NFC technologies that we're looking for.
    private static final String[][] TECH_LISTS = new String[][]{{
            IsoDep.class.getName(),
            NfcA.class.getName(),
    }};
    // The intents that we care about for the above technologies.
    private static final IntentFilter[] FILTERS = new IntentFilter[]{
            new IntentFilter(NfcAdapter.ACTION_TECH_DISCOVERED),
    };
    // The tag used in log messages.
    private static final String TAG = "TerminalEmulator";

    // The intent stored to be used on application resume.
    private PendingIntent pendingIntent;
    private NfcAdapter adapter;
    private ServerSocket serverSocket;
    private TerminalSocketWrapper tsw;

    // Whether or not a current connection to an NFC device is active.
    public boolean isOpen = false;
    // The GUI field display the current program status.
    public TextView statusTextView;

    /**
     * Create a new server socket on the specified port.
     * @return: The new server socket.
     */
    private ServerSocket createServerSocket() {
        ServerSocket s = null;
        try {
            Log.d(TAG, "Creating ServerSocket...");
            s = new ServerSocket();
            s.setReuseAddress(true);
            s.bind(new InetSocketAddress(PORT));
            Log.d(TAG, "Created ServerSocket.");
        } catch (IOException e) {
            Log.e(TAG, "An error was encountered with the server socket:");
            Log.e(TAG, e.getMessage());
            System.exit(1);
        }
        return s;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate.");
        setContentView(R.layout.activity_main);

        // Display an IPv4 address of the phone (the first one) in the GUI.
        TextView ipView = findViewById(R.id.ipBox);
        ipView.setText(Utils.getIpv4Address());

        // Display the program status on the GUI.
        statusTextView = findViewById(R.id.textBox);

        adapter = NfcAdapter.getDefaultAdapter(this);
        serverSocket = createServerSocket();

        // Set up the IP refresh button.
        Button clickButton = findViewById(R.id.button);
        clickButton.setOnClickListener(v -> ipView.setText(Utils.getIpv4Address()));

        setUpIntent();
    }

    @Override
    protected void onResume() {
        Log.d(TAG, "onResume.");
        super.onResume();
        adapter.enableForegroundDispatch(this, pendingIntent, FILTERS, TECH_LISTS);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        Log.d(TAG, "onNewIntent.");
        super.onNewIntent(intent);
        resolveIntent(intent);
    }

    /**
     * Set up the pending intent to be used on application resume.
     */
    public void setUpIntent() {
        Log.d(TAG, "Setting up Intent.");
        Intent intent = new Intent(this,
                getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);
        Intent newIntent = getIntent();
        resolveIntent(newIntent);
        Log.d(TAG, "Set up Intent.");
    }

    /**
     * Handle the event when an intent requires resolving.
     *
     * Upon NFC connection, if we don't already have a connection 'open', set up a new socket to
     * transmit communications.
     * @param intent: The intent to be resolved.
     */
    private void resolveIntent(Intent intent) {
        Log.d(TAG, "Resolving Intent.");
        if (NfcAdapter.ACTION_TECH_DISCOVERED.equals(intent.getAction()) && !isOpen) {
            isOpen = true;
            Transceiver trans = new Transceiver(intent);
            statusTextView.setText("Made NFC connection to card.");
            Log.d(TAG, "Created transceiver");
            tsw = new TerminalSocketWrapper(serverSocket,
                    // Function that takes status update string.
                    (String str) -> runOnUiThread(() -> statusTextView.setText(str)),
                    // Callback function, triggered when connection finishes.
                    () -> this.isOpen = false,
                    trans);
            // Start the thread listening for commands.
            Log.d(TAG, "Created TSW");
            tsw.run();
            Log.d(TAG, "Started TSW");
        }
    }


}

