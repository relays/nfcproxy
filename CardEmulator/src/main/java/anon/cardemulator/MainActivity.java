package anon.cardemulator;

import android.content.Intent;
import android.nfc.NfcAdapter;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import anon.nfccomm.Utils;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = "CardEmulator";
    private NfcAdapter nfcAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //By blake - swapped these two assignments
        setContentView(R.layout.activity_main);
        nfcAdapter = NfcAdapter.getDefaultAdapter(this);


        // Display an IPv4 address of the phone (the first one) in the GUI.
        TextView ipView = findViewById(R.id.ipBox);
        ipView.setText(Utils.getIpv4Address());

        // Set up the IP refresh button.
        Button clickButton = findViewById(R.id.button);
        clickButton.setOnClickListener(v -> ipView.setText(Utils.getIpv4Address()));

        // Start the service transceiving data.
        Intent intent = new Intent(this, CardEmulatorService.class);

        //startActivity(intent);
        //startForegroundService(intent);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(intent);
        } else {
            startService(intent);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        //changed from .enableReaderMode() to .enableForegroundDispatch() to give this activity precedence, stoping other apps from interfacing with the reader.
        //nfcAdapter.enableForegroundDispatch(this, new PendingIntent(), )

        /*nfcAdapter.enableReaderMode(this, (x) -> {

        }, NfcAdapter.FLAG_READER_NFC_A | NfcAdapter.FLAG_READER_SKIP_NDEF_CHECK, null);
*/
        Log.d(TAG, "onResume enabled reader mode");
    }

    @Override
    protected void onPause() {
        super.onPause();
        // We don't want to disable the reader when the app is closed...
        //nfcAdapter.disableReaderMode(this);
        //Log.d(TAG, "onPause disabled reader mode");
    }
}
