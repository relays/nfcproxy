package anon.cardemulator;

/**
 * General shared utilities.
 */
public class Utils {
    private static final char[] HEX_CHARS = "0123456789ABCDEF".toCharArray();

    /**
     * Convert a byte array to its hex string representation.
     * @param bytes: The byte array to convert.
     * @return: Its hex representation.
     */
    public static String byteArrayToHexString(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = HEX_CHARS[v >>> 4];
            hexChars[j * 2 + 1] = HEX_CHARS[v & 0x0F];
        }
        return new String(hexChars);
    }

    /**
     * Convert a hex string to its byte array representation.
     * @param s: The hex string to convert.
     * @return: Its byte array representation.
     */
    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }
}
