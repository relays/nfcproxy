package anon.cardemulator;

import android.os.AsyncTask;
import android.util.Log;

import anon.nfccomm.SocketWrapper;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import java9.util.function.Consumer;

/**
 * A specific socket wrapper for use by the card emulator.
 */
public class CardSocketWrapper extends SocketWrapper {
    // The function called with data to be sent to the terminal.
    private final Consumer<MyByteArray> sendToTerminal;
    // A queue to store data received from the terminal.
    public BlockingQueue<MyByteArray> receivedFromTerminal = new LinkedBlockingQueue<>();

    CardSocketWrapper(ServerSocket serverSocket, Consumer<String> statusFunc, Consumer<MyByteArray> sendToTerminal) {
        // The no-op lambda is the function to be run when the socket is closed (nothing required in
        // this case).
        super(serverSocket, statusFunc, () -> {

        });

        this.sendToTerminal = sendToTerminal;
    }

    @Override
    protected synchronized void waitForConn() {
        super.waitForConn();
        statusFunc.accept("Remote connection made. Ready for communication.");
    }

    /**
     * Send data to the server, and return its reply.
     * @param data: The data to be sent to the server.
     * @return: The reply received from the server.
     */
    private byte[] serverTransceive(byte[] data) {
        try {
            send(data);
            return receive();
        } catch (IOException e) {
            Log.e(TAG, "There was an error while attempting to "
                    + " communicate with the server:");
            Log.e(TAG, e.getMessage());
            return null;
        }
    }

    /**
     * Add a command that has been received from the terminal to the queue, to be sent to the
     * server.
     * @param data: The command received.
     */
    void processTerminalCmd(byte[] data) {
        Log.d(TAG,"terminalCmd to be processed");
        receivedFromTerminal.add(new MyByteArray(data));
    }

    /**
     * The main task to be run in the background, transceiving commands and responses.
     */
    void run() {

        class CardAsyncTask extends AsyncTask<Void, Void, Void> {
            @Override
            public Void doInBackground(Void... params) {
                try {

                    while (true) {
                        byte[] command = receivedFromTerminal.take().data;

                        if (command == null) {
                            Log.i(TAG, "Received empty command - clearing.");
                            receivedFromTerminal.clear();
                            //// Break out of receiving commands for this connection.
                            //break;
                        }
                        Log.d(TAG, "Received command of length "
                                + command.length);
                        byte[] response = serverTransceive(command);
                        if (response == null) {
                            Log.i(TAG, "Received null from server. Attempting to restart connection.");
                            close();
                            open();
                            continue;
                        }
                        sendToTerminal.accept(new MyByteArray(response));
                    }
                } catch (InterruptedException e) {
                    Log.e(TAG, e.getMessage());
                }
                return null;
            }
        }
        new CardAsyncTask().execute();
    }

    /**
     * Erase all messages received from the terminal from the queue.
     */
    public void clearReceivedFromTerminal() {
        receivedFromTerminal.clear();
    }
}


