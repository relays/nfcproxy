package anon.cardemulator;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.graphics.Color;
import android.nfc.cardemulation.HostApduService;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import java.io.IOException;
import java.net.ServerSocket;

import java9.util.function.Consumer;

public class CardEmulatorService extends HostApduService {
    // The port on which to open the server socket.
    private static final int PORT = 59556;
    // The tag used in log messages.
    private static final String TAG = "CardEmulatorService";

    private CardSocketWrapper cardSocketWrapper;
    private ServerSocket serverSocket;

    // The function called with a new program status (as a message string).
    public Consumer<String> statusFunc = null;

    /**
     * Update the program's status with a new status message.
     *
     * @param status: The status message.
     */
    private void updateStatus(String status) {
        // Check that our status function has already been set.
        if (statusFunc != null) {
            statusFunc.accept(status);
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();

        try {
            serverSocket = new ServerSocket(PORT);
            Log.d(TAG, "Created ServerSocket.");
        } catch (IOException e) {
            Log.e(TAG, "An error was encountered with the server socket:");
            Log.e(TAG, e.getMessage());
            System.exit(1);
        }


       cardSocketWrapper = new CardSocketWrapper(serverSocket, (x) -> Log.i(TAG, x), (x) -> sendResponseApdu(x.data));
       // Log.d(TAG, "Preping response object."+ cardSocketWrapper.receivedFromTerminal.toString());
        cardSocketWrapper.run();
        Log.d(TAG, "Created CardSocketWrapper.");

        try {
            // This allows the process to run in the foreground without being killed.

            String NOTIFICATION_CHANNEL_ID = "anon.cardemulator";
            String channelName = "CardEmulatorService Background Service";
            NotificationChannel chan = new NotificationChannel(NOTIFICATION_CHANNEL_ID, channelName, NotificationManager.IMPORTANCE_NONE);
            chan.setLightColor(Color.BLUE);
            chan.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
            NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            assert manager != null;
            manager.createNotificationChannel(chan);

            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);
            Notification notification = notificationBuilder.setOngoing(true)
                    .setSmallIcon(R.drawable.ic_launcher_background)
                    .setContentTitle("CardEmulatorService is running in background")
                    .setPriority(NotificationManager.IMPORTANCE_MIN)
                    .setCategory(Notification.CATEGORY_SERVICE)
                    .build();
            startForeground(PORT, notification);


            Log.d(TAG, "It did the start foreground service");
        } catch (Exception e) {
            Log.d(TAG, "It was unsuccessful in starting foreground service");
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        try {
            if (!serverSocket.isClosed()) {
                serverSocket.close();
            }
        } catch (IOException e) {
            Log.e(TAG, "An error was encountered while closing the server socket:");
            Log.e(TAG, e.getMessage());
        }
        Log.d(MainActivity.TAG, "service destruction");
        updateStatus("CardEmulatorService destroyed.");
    }


    @Override
    public byte[] processCommandApdu(byte[] commandApdu, Bundle extras) {
        Log.d(MainActivity.TAG, "processCommandApdu called");

        //return STATUS_FAILED if the apdu is null
        if (commandApdu == null){
            return Utils.hexStringToByteArray("6D00");
        }

        // DEBUG
        String cmd = Utils.byteArrayToHexString(commandApdu);
        Log.d(MainActivity.TAG, "Command received was: " + cmd);
        // DEBUG
        cardSocketWrapper.processTerminalCmd(commandApdu);

        //return cardSocketWrapper.serverTransceive(commandApdu);
        // The CardSocketWrapper async task will call sendResponseApdu when a response is available.
        // For now, return null.
        return null;
    }

    @Override
    public void onDeactivated(int reason) {
        Log.d(TAG, "NFC service deativated: "+ reason);
        //cardSocketWrapper.close();
        // Clear the queue of messages to purge the connection.
        cardSocketWrapper.clearReceivedFromTerminal();
        updateStatus("NFC connection deactivated.");
    }
}
