package anon.nfccomm;

import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.ByteBuffer;

import java9.util.function.Consumer;

/**
 * A wrapper around a server socket, allowing coordinated send and receive of messages in
 * length-value format.
 */
public abstract class SocketWrapper {
    protected Consumer<String> statusFunc;
    private final ServerSocket serverSocket;
    protected final Runnable finishedFunc;
    private Socket socket;
    protected static final String   TAG = "Emulator";

    public SocketWrapper(ServerSocket serverSocket, Consumer<String> statusFunc,
                         Runnable finishedFunc) {
        this.serverSocket = serverSocket;
        this.statusFunc = statusFunc;
        this.finishedFunc = finishedFunc;
    }

    /**
     * Update the program status with the given status string.
     * @param status: The new program status.
     */
    private void updateStatus(String status) {
        if (statusFunc != null) {
            statusFunc.accept(status);
        }
    }

    /**
     * Block until a connection has been made to the server socket. Only one connection at a time
     * is permitted.
     */
    protected synchronized void waitForConn() {
        if (socket != null && !socket.isClosed()) {
            // Don't allow more than one simultaneous connection.
            return;
        }
        try {
            Log.i(TAG, "Waiting for connection");
            socket = serverSocket.accept();
            Log.i(TAG, "Connection established");

        } catch (IOException e) {
            Log.e(TAG, "An error was encountered with the socket:");
            Log.e(TAG, e.getMessage());
            System.exit(1);
        }
    }

    /**
     * Receive a message from the client.
     * @return: The message received.
     * @throws IOException: on communication error.
     */
    protected synchronized byte[] receive() throws IOException {
        if (socket == null || socket.isClosed()) {
            // Block until we have a new connection started.
            waitForConn();
        }
        InputStream input = socket.getInputStream();
        // 4 bytes to store one integer.
        byte[] lenBuf = new byte[4];
        int res = input.read(lenBuf);
        if (res < 0) {
            Log.d(TAG, "Couldn't receive from socket.");
            return null;
        }
        if (res != lenBuf.length) {
            throw new IOException(String.format(
                    "Attempted to read %d byte(s) for the transmission length; actually read %d.",
                    lenBuf[0], res));
        }
        ByteBuffer bb = ByteBuffer.wrap(lenBuf);
        int len = bb.getInt();
        Log.d(TAG, "Socket (from server) received message length " + len);
        byte[] buffer = new byte[len];
        res = input.read(buffer);
        if (res != buffer.length) {
            throw new IOException(String.format(
                    "Attempted to read %d byte(s) for the transmission; actually read %d.",
                    len, res));
        }
        //Log.d(TAG, "message was: "+ Utils.byteArrayToHexString(res));
        return buffer;
    }

    protected synchronized byte[] cardReceive() throws IOException {
        if (socket == null || socket.isClosed()) {
            // Block until we have a new connection started.
            waitForConn();
        }
        InputStream input = socket.getInputStream();
        // 4 bytes to store one integer.
        byte[] lenBuf = new byte[4];
        int res = input.read(lenBuf, 3, 1);
        if (res < 0) {
            Log.d(TAG, "Couldn't receive from socket.");
            return null;
        }
        /*if (res != lenBuf.length) {
            throw new IOException(String.format(
                    "Attempted to read %d byte(s) for the transmission length; actually read %d.",
                    lenBuf[0], res));
        }*/
        ByteBuffer bb = ByteBuffer.wrap(lenBuf);
        int len = bb.getInt();
        Log.d(TAG, "Socket (from server) received message length " + len);
        byte[] buffer = new byte[len];
        res = input.read(buffer);
        if (res != buffer.length) {
            throw new IOException(String.format(
                    "Attempted to read %d byte(s) for the transmission; actually read %d.",
                    len, res));
        }
        return buffer;
    }

    /**
     * Send a message to the client.
     * @param data: The data to send.
     * @throws IOException: on communication error.
     */
    protected synchronized void send(byte[] data) throws IOException {
        if (socket == null || socket.isClosed()) {
            Log.d(TAG,"socket connection is not ready, blocking until ready");
            // Block until a connection is ready.
            waitForConn();
        }
        OutputStream output = socket.getOutputStream();
        Log.d(TAG, "Writing length of data to socket (server) " + data.length);
        byte[] lenBuf = ByteBuffer.allocate(4).putInt(data.length).array();
        Log.d(TAG, "Data to server; length: "+ Utils.byteArrayToHexString(lenBuf) +" body: "+ Utils.byteArrayToHexString(data));
        output.write(data.length);
        //output.write(lenBuf);
        output.write(data);
        output.flush();
    }

    /**
     * Wait for a new connection to occur.
     */
    public synchronized void open() {
        updateStatus("Waiting for remote connection.");
        waitForConn();
        updateStatus("Established remote connection. Ready to communicate.");
    }

    /**
     * Close any current connection to the server socket.
     */
    public void close() {
        try {
            if (socket != null) {
                socket.close();
            }
        } catch (IOException e) {
            Log.e(TAG, "There was an error while attempting to close "
                    + "the socket:");
            Log.e(TAG, e.getMessage());
        }
        updateStatus("Finished communication.");
        finishedFunc.run();
    }

}
