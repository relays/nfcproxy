package anon.nfccomm;

import android.content.Intent;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.IsoDep;
import android.util.Log;

import java.io.IOException;

import static java.lang.System.exit;

/**
 * Send and receive data to/from an NFC IsoDep device.
 */
public class Transceiver {
    private IsoDep isoDep;

    public Transceiver(Intent intent) {
        this.connect(intent);
    }

    /**
     * Establish a connection to a device.
     * @param intent
     */
    private void connect(Intent intent) {
        Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
        Log.d("NFC_PROXY", "tag: " + tag);
        isoDep = IsoDep.get(tag);
        try {
            isoDep.connect();
        } catch (IOException e) {
            Log.e("NFC_PROXY", "Error making IsoDep connection:");
            Log.e("NFC_PROXY", e.getMessage());
            exit(1);
        }
    }

    /**
     * Send a message to the device, and return its response.
     * @param command: The message to send to the device.
     * @return: The device's response.
     */
    public byte[] transceive(byte[] command) {

        byte[] response = null;
        try {

            response = isoDep.transceive(command);
        } catch (IOException e) {
            Log.e("NFC_PROXY", "There was an error while transceiving with the device:");
            Log.e("NFC_PROXY", e.toString());
            exit(1);
        }
        if (response != null) {
            StringBuilder sb = new StringBuilder();
            for (byte b : response) {
                sb.append(String.format("%02X", b));
            }
            Log.d("NFC_PROXY", "recvd from device: " + sb.toString());
        }
        return response;
    }
}
