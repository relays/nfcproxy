# NFCProxy

This repo hosts the original Android Apps relays code from
    [[1]](https://www.cs.bham.ac.uk/~tpc/Papers/stoppingRelays.pdf), containing
    both the `CardEmulator` app and the `TerminalEmulator` app.

## Usage

- original relay apps: 
  * `CardEmulator` -- to be used against a real EMV reader; 
  * `TerminalEmulator` -- to be used against a card;
- requires the relay server `NFCProxy_relay.py` (find it [here](https://gitlab.com/relays/server/-/blob/main/NFCProxy_relay.py))
 
```sh
usage: NFCProxy_relay.py [-h] [-tp TERMPORT] [-cp CARDPORT] TermEmulatorIP CardEmulatorIP

Relay server connecting to a terminal emulator and a card emulator

positional arguments:
  TermEmulatorIP        the IP of the Terminal Emulator APP
  CardEmulatorIP        the IP of the Card Emulator APP

optional arguments:
  -h, --help            show this help message and exit
  -tp TERMPORT, --termPort TERMPORT
                        the port for the Terminal Emulator APP (default: 59557)
  -cp CARDPORT, --cardPort CARDPORT
                        the port for the Card Emulator APP (default: 59556)
```


[1] Chothia, Tom, et al. "Relay cost bounding for contactless EMV payments."
International Conference on Financial Cryptography and Data Security. Springer,
Berlin, Heidelberg, 2015.
